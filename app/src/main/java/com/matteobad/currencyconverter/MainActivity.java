package com.matteobad.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public void convert(View view) {
        EditText editTextValue = (EditText) findViewById(R.id.mainEditText);
        Double valueToConvert = Double.valueOf(editTextValue.getText().toString());
        Double convertedValue = valueToConvert * 0.968;
        Toast.makeText(MainActivity.this, String.format(Locale.ITALY, "� %.2f", convertedValue), Toast.LENGTH_SHORT).show();
        Log.i("info", String.valueOf(valueToConvert));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
